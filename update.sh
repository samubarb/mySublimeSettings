#!/bin/sh
#
# update.sh

SUBL_PATH="$HOME/.config/sublime-text-3"

# Copy User Sublime configs in git folder
echo "Copying $SUBL_PATH/Packages/User/Preferences.sublime-settings"
cp $SUBL_PATH/Packages/User/Preferences.sublime-settings ./

# Copy AStyleFormatter configs in git folder
echo "Copying $SUBL_PATH/Packages/SublimeAStyleFormatter/SublimeAStyleFormatter.sublime-settings"
cp $SUBL_PATH/Packages/SublimeAStyleFormatter/SublimeAStyleFormatter.sublime-settings ./

# Show git status
echo "" # newline
git status ./
