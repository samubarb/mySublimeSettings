#!/bin/sh
#
# set.sh

SUBL_PATH="$HOME/.config/sublime-text-3"

# Copy User Sublime configs in git folder
echo "Copying Preferences.sublime-settings in $SUBL_PATH/Packages/User/"
cp ./Preferences.sublime-settings $SUBL_PATH/Packages/User/

# Copy AStyleFormatter configs in git folder
echo "Copying SublimeAStyleFormatter.sublime-settings in $SUBL_PATH/Packages/SublimeAStyleFormatter/"
cp ./SublimeAStyleFormatter.sublime-settings $SUBL_PATH/Packages/SublimeAStyleFormatter/
